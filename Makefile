NAME = a.out

SRC = 	main.c \
		MiniSDL.c \

INC = -I .

CC = gcc

SDLLIB		=	$(shell sdl2-config --libs)
SDLFLAGS	=	$(shell sdl2-config --cflags)

# gcc -std=c17 main.c -LC:/SDL2/x86_64-w64-mingw32/bin/ -lmingw32 -lSDL2 -mwindows  -I C:/SDL2/x86_64-w64-mingw32/include/SDL2 -Dmain=SDL_main -L/usr/lib/libSDL2main.a

ifeq ($(OS), Windows_NT)
        SDLFLAGS	= -I C:/SDL2/include -mwindows
        SDLLIB  	= -L C:/SDL2/x64/lib -lSDL2main -lSDL2 -mwindows -lmingw32
        CC			= x86_64-w64-mingw32-g++ -std=c++11
endif

all:
	@echo "Compiling Project"
	@$(CC) -o $(NAME) $(SRC) $(INC) $(SDLFLAGS) $(SDLLIB)
	@echo "Project Compiled"

clean:
	@echo "Cleaning object files"

fclean: clean
	@echo "Removing binary file" 
	@/bin/rm -f $(NAME)

re: fclean all

#sh -c "$(curl -fsSL https://raw.githubusercontent.com/Tolsadus/42homebrewfix/master/install.sh)"
