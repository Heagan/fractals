#include "MiniSDL.h"

int width;
int height;

SDL_Window			*window;
SDL_Surface			*surface;
SDL_Renderer		*renderer;
SDL_Event			event;

int init(char *name, int win_width, int win_height) {
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
        return 1;
    }

    if (!(window = SDL_CreateWindow(name, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
    win_width, win_height, SDL_WINDOW_SHOWN))) {
        return 1;
    }

    if (!(renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED))) {
        return 1;
    }
	width = win_width;
	height = win_height;
	return 0;
}

void destroy(){
    SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
}

int display() {
    int nput = handleInput();
	SDL_RenderPresent(renderer);
	return nput;
}

int	mkcolour(unsigned int r, unsigned int g, unsigned int b) {
	float n = (r * 256 * 256) + (g * 256) + b;

	return n;
}

int mkcolour2(int min, int max) {
	min = min ? min : 1;
	max = max ? max : 1;

	float n = (255 * (max / min) * 256 * 256) + (255 * (min / max) * 256);

	return n;
}


void putpixel(unsigned int x, unsigned int y, unsigned int colour) {
	SDL_SetRenderDrawColor(renderer, colour >> 16, (colour >> 8) % 256, colour % 256, 255);
	SDL_RenderDrawPoint(renderer, x, y);
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
}

void drawLine(int x1, int y1, int x2, int y2, int colour) {
	SDL_SetRenderDrawColor(renderer, colour >> 16, (colour >> 8) % 256, colour % 256, 255);
	SDL_RenderDrawLine(renderer, x1, y1, x2, y2);
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
}

void fill(int colour) {
	for (int y = 0; y < height ; y++) {
		for (int x = 0; x < width ; x++) {
			putpixel(x, y, colour);
		}
	}
}

void rect(unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2, unsigned int colour) {
	for (unsigned int y = y1; y <= y2; y++) {
    	for (unsigned int x = x1; x <= x2; x++) {
			putpixel(x, y, colour);
		}
	}
}

void clearpixels( void ) {
	SDL_RenderClear(renderer);
}

int handleInput( void ) {
	int	keyCode = 0;

	while (SDL_PollEvent(&event))
	{
		if (event.type == SDL_QUIT) {
			exit(0);
		}
		if (event.key.state) {
			return keyCode;
		}
		keyCode = event.key.keysym.sym;
		if (keyCode == SDLK_ESCAPE) {
			exit(0);
		}
	}
	return keyCode;
}


void setWinName(char *name) {
	SDL_SetWindowTitle(window, name);
}


void progressbar(float percent) {
	if (!window) {
		init("Percent", 200, 50);
	}
	if (percent > 1) {
		percent = percent / 100;
	}
	rect(0, 0, percent * 200, 50, 255);
	display();
}

double map(double x, double in_min, double in_max, double out_min, double out_max) {
    /*
    Scale n from (x, maxx) to (y, maxy)
    For Example:
    Scale 10 from (0, 100) to (-2, 2)
    */
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

