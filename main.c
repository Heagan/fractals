#include "MiniSDL.h"


void mandelbrot(float zoom = 1) {
    for (double py = 0; py < 500; py++) {
        for (double px = 0; px < 500; px++) {
            
            // These numbers control the zoom level
            double x0 = map(px, 0, 500, -2 * zoom, 0.47 * zoom);
            double y0 = map(py, 0, 500, -1.12 * zoom, 1.12 * zoom);
            
            double x = 0; 
            double y = 0; 
            double iteration = 0;
            double max_iteration = 1000;
            while (x*x + y*y < 2*2 && iteration < max_iteration) {
                double xtemp = x*x - y*y + x0;
                y = 2*x*y + y0;
                x = xtemp;
                iteration = iteration + 1;
            }
            putpixel(px, py, mkcolour2(iteration, max_iteration));
        }
    }
}

/*
If main() doesnt work use WinMain(), seems like its an SDL thing
*/
int WinMain() {
    // https://en.wikipedia.org/wiki/Mandelbrot_set#Computer_drawings

    init("Fractol", 500, 500);
    fill(mkcolour(0, 0, 0));
    
    
    int nput;
    int prev_nput = 0;
    int i = 1;
    while (1) {
        nput = display();
        if (nput != 0 && nput != prev_nput) {
            prev_nput = nput;
            // i *= 0.1 ;
            mandelbrot(1);
        }
    }
    destroy();
}

/*
for each pixel (Px, Py) on the screen do
    x0 := scaled x coordinate of pixel (scaled to lie in the Mandelbrot X scale (-2.00, 0.47))
    y0 := scaled y coordinate of pixel (scaled to lie in the Mandelbrot Y scale (-1.12, 1.12))
    x := 0.0
    y := 0.0
    iteration := 0
    max_iteration := 1000
    while (x*x + y*y ≤ 2*2 AND iteration < max_iteration) do
        xtemp := x*x - y*y + x0
        y := 2*x*y + y0
        x := xtemp
        iteration := iteration + 1
    
    color := palette[iteration]
    plot(Px, Py, color)
*/