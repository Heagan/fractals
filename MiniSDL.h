#ifndef MINI_SDL_H
# define MINI_SDL_H

#include "SDL.h"

// int width;
// int height;

// SDL_Window			*window;
// SDL_Surface			*surface;
// SDL_Renderer		*renderer;
// SDL_Event			event;

int init(char *name, int win_width, int win_height);
void destroy();
int display();
int	mkcolour(unsigned int r, unsigned int g, unsigned int b);
int mkcolour2(int min, int max);
void putpixel(unsigned int x, unsigned int y, unsigned int colour);
void drawLine(int x1, int y1, int x2, int y2, int colour);
void fill(int colour);
void rect(unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2, unsigned int colour);
void clearpixels( void );
int handleInput( void );
void setWinName(char *name);
void progressbar(float percent);
double map(double x, double in_min, double in_max, double out_min, double out_max);

#endif


